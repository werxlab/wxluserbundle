<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace WXL\UserBundle\EventListener;

use WXL\UserBundle\WXLUserEvents;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FlashListener implements EventSubscriberInterface
{
    /**
     * @var string[]
     */
    private static $successMessages = [
        WXLUserEvents::CHANGE_PASSWORD_COMPLETED => 'change_password.flash.success',
        WXLUserEvents::GROUP_CREATE_COMPLETED => 'group.flash.created',
        WXLUserEvents::GROUP_DELETE_COMPLETED => 'group.flash.deleted',
        WXLUserEvents::GROUP_EDIT_COMPLETED => 'group.flash.updated',
        WXLUserEvents::PROFILE_EDIT_COMPLETED => 'profile.flash.updated',
        WXLUserEvents::REGISTRATION_COMPLETED => 'registration.flash.user_created',
        WXLUserEvents::RESETTING_RESET_COMPLETED => 'resetting.flash.success',
    ];

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * FlashListener constructor.
     */
    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            WXLUserEvents::CHANGE_PASSWORD_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::GROUP_CREATE_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::GROUP_DELETE_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::GROUP_EDIT_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::PROFILE_EDIT_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::REGISTRATION_COMPLETED => 'addSuccessFlash',
            WXLUserEvents::RESETTING_RESET_COMPLETED => 'addSuccessFlash',
        ];
    }

    /**
     * @param string $eventName
     */
    public function addSuccessFlash(Event $event, $eventName)
    {
        if (!isset(self::$successMessages[$eventName])) {
            throw new \InvalidArgumentException('This event does not correspond to a known flash message');
        }

        $this->session->getFlashBag()->add('success', $this->trans(self::$successMessages[$eventName]));
    }

    /**
     * @param string$message
     *
     * @return string
     */
    private function trans($message, array $params = [])
    {
        return $this->translator->trans($message, $params, 'WXLUserBundle');
    }
}
