WXLUserBundle
=============

is a fork of the FOSUserBundle, which is not maintained anymore. I plan to keep this updated for the latest LST & Unstable Symfony Framework.


## Documentation

Read the **<a href="https://gitlab.com/werxlab/wxluserbundle/-/wikis/home" target="_blank">wiki page</a>** on how to use this bundle. The first page is a simple and quick howto, other pages will be added at a later point in development.
